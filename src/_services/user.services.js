import axios from 'axios';
import VueJwtDecode from "vue-jwt-decode";
import router from "../_router/index.js";

export const userService = {
    login,
    logout,
    getRoleUser
}

function login(username, password) {

    return axios.post(
        `${process.env.VUE_APP_API_URL}/login`,
        {
            "username": username.toString(),
            "password": password.toString()
        }).then(
            response => {
                if (response.data.token) {
                    localStorage.setItem('token', JSON.stringify(response.data.token));
                    return response.data;
                } else return false;
            }
        );
}

function logout() {
    localStorage.removeItem('token');
    router.push('login');
}

function getRoleUser() {
    let token = VueJwtDecode.decode(JSON.parse(localStorage.getItem('token')));
    return token.role;
}