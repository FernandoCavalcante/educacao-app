import { userService } from '../_services/user.services.js';

const token = JSON.parse(localStorage.getItem('token'));

const initialState = token ? { status: { loggedIn: true }, token } : { status: { loggedIn: false }, user: null }

export const authentication = {
    namespaced: true,
    state: initialState,
    getters: {
        status: state => {
            return state.status;
        }
    },
    actions: {
        async login({ commit }, user) {
            await userService.login(user.username, user.password)
                .then(
                    data => {
                        if (data != false) {
                            commit('loginSuccess', data);
                        } else
                            commit('loginFailure')
                    },
                    error => {
                        commit('loginFailure', error);
                    }
                );
        },
        logout({ commit }) {

            commit('logout');
            userService.logout()
        }
    },
    mutations: {
        loginSuccess(state, user) {
            state.status = { loggedIn: true };
            state.user = user;
        },
        loginFailure(state) {
            state.status = { loggedIn: false };
            state.user = {};
        },
        logout(state) {
            state.status = {};
            state.user = null;
        }
    }
}
