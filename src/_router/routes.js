import Home from '../views/HomePage/Home.vue'
import Login from "../views/Login/Login.vue";
import PasswordRecovery from "../views/Login/PasswordRecovery.vue";
import Register from "../views/Login/Register.vue";


export const routes = [
    {
        path: '/home',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/recover',
        name: 'PasswordRecovery',
        component: PasswordRecovery
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    }
]