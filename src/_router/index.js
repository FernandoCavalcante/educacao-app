import Vue from 'vue'
import VueRouter from 'vue-router'

import { routes } from "./routes.js";
import { store } from "../_store/index";

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let loggedIn = store.getters["authentication/status"].loggedIn && localStorage.getItem('token');

  if (to.name != 'Login' && to.name != 'Register' && to.name != 'PasswordRecovery')
    if (!loggedIn) next('/login');
    else next();
  else next();

})

export default router
